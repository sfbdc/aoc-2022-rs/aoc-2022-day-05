fn main() {
    let input = include_str!("../input.txt");
    println!("Solution A: {}", solve_for_a(input));
    println!("Solution B: {}", solve_for_b(input));
}

fn get_stacks(input: &str) -> (Vec<Vec<char>>, usize) {
    let stacks_lines: Vec<&str> = input
        .lines()
        .take_while(|line| !line.contains(|c: char| c.is_numeric()))
        .collect();
    let stack_diagram_lines = stacks_lines.len();
    let stacks_count: u32 = (stacks_lines[0].len() as u32 + 1) / 4;
    let mut stacks = vec![Vec::<char>::new(); stacks_count as usize];

    for line in stacks_lines.iter().rev() {
        for (j, ch) in line.chars().enumerate() {
            if ch.is_alphabetic() {
                let stack_idx: usize = j / 4;
                stacks[stack_idx].push(ch);
            }
        }
    }

    (stacks, stack_diagram_lines)
}

#[test]
fn test_solve_for_a() {
    let test_input = include_str!("../test.txt");
    assert_eq!(solve_for_a(test_input), "CMZ");
}

fn solve_for_a(input: &str) -> String {
    let (mut stacks, stack_diagram_lines) = get_stacks(input);
    let commands: Vec<&str> = input.lines().skip(stack_diagram_lines + 2).collect();
    for command in commands {
        let command_arguments: Vec<u32> = command
            .split_whitespace()
            .flat_map(|s| s.parse::<u32>())
            .collect();
        if let [how_many, from_stack, to_stack, ..] = command_arguments.as_slice() {
            for _ in 0..*how_many {
                if let Some(crate_moving) = stacks[*from_stack as usize - 1].pop() {
                    stacks[*to_stack as usize - 1].push(crate_moving);
                }
            }
        }
    }

    let mut solution = String::new();
    for mut stack in stacks {
        if let Some(top_crate) = stack.pop() {
            solution.push(top_crate);
        }
    }

    solution
}

#[test]
fn test_solve_for_b() {
    let test_input = include_str!("../test.txt");
    assert_eq!(solve_for_b(test_input), "MCD");
}

fn solve_for_b(input: &str) -> String {
    let (mut stacks, stack_diagram_lines) = get_stacks(input);
    let commands: Vec<&str> = input.lines().skip(stack_diagram_lines + 2).collect();
    for command in commands {
        let command_arguments: Vec<u32> = command
            .split_whitespace()
            .flat_map(|s| s.parse::<u32>())
            .collect();
        if let [how_many, from_stack, to_stack, ..] = command_arguments.as_slice() {
            let splice_index = stacks[*from_stack as usize - 1].len() - *how_many as usize;
            let mut moving_substack = stacks[*from_stack as usize - 1].split_off(splice_index);
            stacks[*to_stack as usize - 1].append(&mut moving_substack);
        }
    }

    let mut solution = String::new();
    for mut stack in stacks {
        if let Some(top_crate) = stack.pop() {
            solution.push(top_crate);
        }
    }

    solution
}
